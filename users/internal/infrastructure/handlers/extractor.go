package handlers

import (
	"net/http"
	"strconv"

	"users/internal/infrastructure/errors"
	"users/internal/infrastructure/middleware"
	"users/internal/infrastructure/tools/cryptography"
)

func ExtractUser(r *http.Request) (cryptography.UserFromClaims, error) {
	ctx := r.Context()
	u, ok := ctx.Value(middleware.UserRequest{}).(cryptography.UserClaims)
	if !ok {
		return cryptography.UserFromClaims{}, errors.TokenExtractUserError
	}
	userID, err := strconv.Atoi(u.ID)
	if err != nil {
		return cryptography.UserFromClaims{}, errors.TokenExtractUserError
	}
	role, err := strconv.Atoi(u.Role)
	if err != nil {
		return cryptography.UserFromClaims{}, errors.TokenExtractUserError
	}

	return cryptography.UserFromClaims{
		ID:     userID,
		Role:   role,
		Groups: nil,
		Layers: nil,
	}, nil
}
