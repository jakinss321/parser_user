package router

import (
	"net/http"
	"users/internal/infrastructure/component"
	"users/internal/infrastructure/middleware"
	"users/internal/modules"

	"github.com/go-chi/chi/v5"
)

func NewApiRouter(controllers *modules.Controllers, components *component.Components) http.Handler {
	r := chi.NewRouter()

	r.Route("/api", func(r chi.Router) {
		r.Route("/1", func(r chi.Router) {
			authCheck := middleware.NewTokenManager(components.Responder, components.TokenManager)
			r.Route("/auth", func(r chi.Router) {
				authController := controllers.Auth
				userController := controllers.User
				r.Post("/register", authController.Register)
				r.Post("/login", authController.Login)
				r.Post("/verify", authController.Verify)
					r.With(authCheck.CheckStrict).Route("/user", func(r chi.Router) {
					r.Get("/", userController.Profile)
				})
				r.With(authCheck.CheckStrict).Route("/change", func(r chi.Router) {
					r.Post("/", userController.ChangePassword)
				})
			

				r.Route("/refresh", func(r chi.Router) {
					r.Use(authCheck.CheckRefresh)
					r.Post("/", authController.Refresh)
				})
				
			})
		})
	})

	return r
}
