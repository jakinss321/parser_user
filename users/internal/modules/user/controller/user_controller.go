package controller

import (
	"net/http"

	"users/internal/infrastructure/component"
	"users/internal/infrastructure/errors"
	"users/internal/infrastructure/handlers"
	"users/internal/infrastructure/responder"
	"users/internal/modules/user/service"

	"github.com/ptflp/godecoder"
)

type Userer interface {
	Profile(http.ResponseWriter, *http.Request)
	GetUsersInfo(http.ResponseWriter, *http.Request)
	ChangePassword(w http.ResponseWriter, r *http.Request)
}

type User struct {
	service service.Userer
	responder.Responder
	godecoder.Decoder
}

func NewUser(service service.Userer, components *component.Components) Userer {
	return &User{service: service, Responder: components.Responder, Decoder: components.Decoder}
}

func (u *User) Profile(w http.ResponseWriter, r *http.Request) {
	claims, err := handlers.ExtractUser(r)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}
	out := u.service.GetByID(r.Context(), service.GetByIDIn{UserID: claims.ID})
	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, ProfileResponse{
			ErrorCode: out.ErrorCode,
			Data: Data{
				Message: "retrieving user error",
			},
		})
		return
	}

	u.OutputJSON(w, ProfileResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: Data{
			User: *out.User,
		},
	})
}

func (u *User) ChangePassword(w http.ResponseWriter, r *http.Request) {
	// Извлекаем данные из запроса
	var in service.ChangePasswordIn
	if err := u.Decode(r.Body, &in); err != nil {
		u.ErrorBadRequest(w, err)
		return
	}

	// Вызываем метод сервиса для изменения пароля пользователя
	out := u.service.ChangePassword(r.Context(), in)
	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, ChangePasswordResponse{
			ErrorCode: out.ErrorCode,
			Data: ChangePasswordData{
				Message: "changing password error",
			},
		})
		return
	}

	// Возвращаем успешный результат
	u.OutputJSON(w, ChangePasswordResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: ChangePasswordData{
			Message: "password changed",
		},
	})
}

func (u *User) GetUsersInfo(w http.ResponseWriter, r *http.Request) {
	panic("implement me")
}
