package modules

import (
	"users/internal/infrastructure/component"
	aservice "users/internal/modules/auth/service"
	uservice "users/internal/modules/user/service"
	"users/internal/storages"
)

type Services struct {
	User uservice.Userer
	Auth aservice.Auther
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	userService := uservice.NewUserService(storages.User, components.Logger)
	return &Services{
		User: userService,
		Auth: aservice.NewAuth(userService, storages.Verify, components),
	}
}
