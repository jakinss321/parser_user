package db

import (
	"database/sql"
	"fmt"
	"time"
	"users/config"
	"users/internal/db/adapter"
	"users/internal/infrastructure/db/scanner"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"go.uber.org/zap"
)

func NewSqlDB(dbConf config.DB, scanner scanner.Scanner, logger *zap.Logger) (*sqlx.DB, *adapter.SQLAdapter, error) {
	dsn := "host=db1 port=5432 user=postgres dbname=postgres password=test sslmode=disable"
	// dsn := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", dbConf.Host, dbConf.Port, dbConf.User, dbConf.Password, dbConf.Name)
	dbRaw, err := sql.Open(dbConf.Driver, dsn)
	if err != nil {
		return nil, nil, err
	}

	ticker := time.NewTicker(1 * time.Second)
	defer ticker.Stop()
	timeoutExceeded := time.After(time.Second * time.Duration(dbConf.Timeout))

	for {
		select {
		case <-timeoutExceeded:
			return nil, nil, fmt.Errorf("db connection failed after %d timeout %s", dbConf.Timeout, err)
		case <-ticker.C:
			err = dbRaw.Ping()
			if err == nil {
				db := sqlx.NewDb(dbRaw, dbConf.Driver)
				db.SetMaxOpenConns(50)
				db.SetMaxIdleConns(50)
				sqlAdapter := adapter.NewSqlAdapter(db, dbConf, scanner)
				return db, sqlAdapter, nil
			}
			logger.Error("failed to connect to the database", zap.String("dsn", dsn), zap.Error(err))
		}
	}
}
