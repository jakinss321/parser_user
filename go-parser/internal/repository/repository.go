package repository

import (
	"errors"
	"main/models"
	"os"

	"github.com/jmoiron/sqlx"
	"go.mongodb.org/mongo-driver/mongo"
)

type VacancyRepository interface {
	CreateVacancy(vacancy *models.Vacancy) error
	GetVacancy(id int) (*models.Vacancy, error)
	GetAllVacancies() ([]*models.Vacancy, error)
	UpdateVacancy(id int, vacancy *models.Vacancy) error
	DeleteVacancy(id int) error
}

type Repository struct {
	postgres VacancyRepository
	mongodb  VacancyRepository
}

func NewRepository(postgresDB *sqlx.DB, mongoClient *mongo.Client, mongoCollectionName *mongo.Collection) (VacancyRepository, error) {
	var postgresRepo *PostgresRepository
	var mongoRepo VacancyRepository
	switch os.Getenv("DB") {
	case "postgres":
		postgresRepo = NewPostgresRepository(postgresDB)
	case "mongo":
		mongoRepo, _ = NewMongoDBRepository(mongoClient, mongoCollectionName.Name(), mongoCollectionName)

	}
	return &Repository{
		postgres: postgresRepo,
		mongodb:  mongoRepo,
	}, nil

}

func (r *Repository) CreateVacancy(vacancy *models.Vacancy) error {
	switch os.Getenv("DB") {
	case "postgres":
		return r.postgres.CreateVacancy(vacancy)
	case "mongo":
		return r.mongodb.CreateVacancy(vacancy)
	}
	return errors.New("no database connection")
}

func (r *Repository) GetVacancy(id int) (*models.Vacancy, error) {
	switch os.Getenv("DB") {
	case "postgres":
		return r.postgres.GetVacancy(id)
	case "mongo":
		return r.mongodb.GetVacancy(id)
	}
	return nil, errors.New("no database connection")
}

func (r *Repository) GetAllVacancies() ([]*models.Vacancy, error) {
	switch os.Getenv("DB") {
	case "postgres":
		return r.postgres.GetAllVacancies()
	case "mongo":
		return r.mongodb.GetAllVacancies()
	}
	return nil, errors.New("no database connection")
}

func (r *Repository) UpdateVacancy(id int, vacancy *models.Vacancy) error {
	switch os.Getenv("DB") {
	case "postgres":
		return r.postgres.UpdateVacancy(id, vacancy)
	case "mongo":
		return r.mongodb.UpdateVacancy(id, vacancy)
	}
	return errors.New("no database connection")
}

func (r *Repository) DeleteVacancy(id int) error {
	switch os.Getenv("DB") {
	case "postgres":
		return r.postgres.DeleteVacancy(id)
	case "mongo":
		return r.mongodb.DeleteVacancy(id)
	}
	return errors.New("no database connection")
}
