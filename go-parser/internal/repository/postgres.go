package repository

import (
	"database/sql"
	"fmt"
	"main/models"
	"strings"

	"github.com/jmoiron/sqlx"
)

const (
	vacancyTable = "vacancyTable"
)

type Config struct {
	Host           string
	Port           string
	Username       string
	Password       string
	DBName         string
	SSLMode        string
	CollectionName string
}

func NewPostgresDB(cfg Config) (*sqlx.DB, error) {
	db, err := sqlx.Open("postgres", fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=%s",
		cfg.Host, cfg.Port, cfg.Username, cfg.DBName, cfg.Password, cfg.SSLMode))
	if err != nil {
		return nil, err
	}

	err = db.Ping()

	if err != nil {
		return nil, err
	}
	return db, nil
}

type PostgresRepository struct {
	db *sqlx.DB
}

func NewPostgresRepository(db *sqlx.DB) *PostgresRepository {
	return &PostgresRepository{db: db}
}

func (r *PostgresRepository) CreateVacancy(vacancy *models.Vacancy) error {
	var id int
	query := fmt.Sprintf(`INSERT INTO %s (context, type, date_posted, title, description, identifier, valid_through, hiring_organization, job_location, job_location_type, employment_type) 
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11) RETURNING id`, vacancyTable)
	row := r.db.QueryRow(query, vacancy.Context, vacancy.Type, vacancy.DatePosted, vacancy.Title, vacancy.Description, vacancy.Identifier, vacancy.ValidThrough, vacancy.HiringOrganization, vacancy.JobLocation, vacancy.JobLocationType, vacancy.EmploymentType)
	if err := row.Scan(&id); err != nil {
		return err
	}
	return nil
}

func (r *PostgresRepository) DeleteVacancy(id int) error {
	query := fmt.Sprintf("DELETE FROM %s WHERE id=$1", vacancyTable)

	result, err := r.db.Exec(query, id)
	if err != nil {
		return fmt.Errorf("failed to execute delete query: %s", err)
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return fmt.Errorf("failed to get rows affected: %s", err)
	}

	if rowsAffected == 0 {
		return fmt.Errorf("vacancy with ID %d not found", id)
	}

	return nil
}

func (r *PostgresRepository) GetAllVacancies() ([]*models.Vacancy, error) {
	var vacancies []*models.Vacancy
	query := fmt.Sprintf("SELECT * FROM %s", vacancyTable)
	err := r.db.Select(&vacancies, query)
	return vacancies, err
}

func (r *PostgresRepository) GetVacancy(id int) (*models.Vacancy, error) {
	query := fmt.Sprintf("SELECT id, context, type, date_posted, title, description, identifier, valid_through, hiring_organization, job_location, job_location_type, employment_type FROM %s WHERE id = $1", vacancyTable)
	item := &models.Vacancy{}
	err := r.db.QueryRow(query, id).Scan(&item.ID, &item.Context, &item.Type, &item.DatePosted, &item.Title, &item.Description, &item.Identifier, &item.ValidThrough, &item.HiringOrganization, &item.JobLocation, &item.JobLocationType, &item.EmploymentType)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, fmt.Errorf("vacancy with ID %d not found", id)
		}
		return nil, fmt.Errorf("failed to execute select query: %s", err)
	}
	return item, nil
}

func (r *PostgresRepository) UpdateVacancy(id int, item *models.Vacancy) error {
	setValues := make([]string, 0)
	args := make([]interface{}, 0)
	argId := 1

	if item.Context != "" {
		setValues = append(setValues, fmt.Sprintf("context=$%d", argId))
		args = append(args, item.Context)
		argId++
	}

	if item.Type != "" {
		setValues = append(setValues, fmt.Sprintf("type=$%d", argId))
		args = append(args, item.Type)
		argId++
	}

	if item.DatePosted != "" {
		setValues = append(setValues, fmt.Sprintf("date_posted=$%d", argId))
		args = append(args, item.DatePosted)
		argId++
	}

	if item.Title != "" {
		setValues = append(setValues, fmt.Sprintf("title=$%d", argId))
		args = append(args, item.Title)
		argId++
	}

	if item.Description != "" {
		setValues = append(setValues, fmt.Sprintf("description=$%d", argId))
		args = append(args, item.Description)
		argId++
	}

	if item.Identifier != "" {
		setValues = append(setValues, fmt.Sprintf("identifier=$%d", argId))
		args = append(args, item.Identifier)
		argId++
	}

	if item.ValidThrough != "" {
		setValues = append(setValues, fmt.Sprintf("valid_through=$%d", argId))
		args = append(args, item.ValidThrough)
		argId++
	}

	if item.HiringOrganization != "" {
		setValues = append(setValues, fmt.Sprintf("hiring_organization=$%d", argId))
		args = append(args, item.HiringOrganization)
		argId++
	}

	if item.JobLocation != "" {
		setValues = append(setValues, fmt.Sprintf("job_location=$%d", argId))
		args = append(args, item.JobLocation)
		argId++
	}

	if item.JobLocationType != "" {
		setValues = append(setValues, fmt.Sprintf("job_location_type=$%d", argId))
		args = append(args, item.JobLocationType)
		argId++
	}

	if item.EmploymentType != "" {
		setValues = append(setValues, fmt.Sprintf("employment_type=$%d", argId))
		args = append(args, item.EmploymentType)
		argId++
	}

	args = append(args, item.ID)

	setQuery := strings.Join(setValues, ", ")
	query := fmt.Sprintf("UPDATE %s SET %s WHERE id=$%d", vacancyTable, setQuery, argId)

	_, err := r.db.Exec(query, args...)
	return err
}
