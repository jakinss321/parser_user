package handler

import (
	"main/internal/component"
	"main/internal/middleware"
	"main/internal/service"
	"main/internal/userclient"

	_ "main/docs"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

type Handler struct {
	services       service.Services
	authMiddleware *middleware.AuthMiddleware
}

func NewHandler(services service.Services, responder *component.Responder, authServiceClient *userclient.UsersClient) *Handler {
	return &Handler{
		services:       services,
		authMiddleware: middleware.NewAuthMiddleware(responder, authServiceClient),
	}
}

func (h *Handler) InitRoutes() *gin.Engine {
	router := gin.New()

	// Защищенные роуты
	authorized := router.Group("/")
	authorized.Use(h.authMiddleware.CheckStrict())
	{
		authorized.POST("/vacancy", h.createVacancy)
		authorized.GET("/vacs", h.getAllVacancies)
		authorized.GET("/vacancy/:id", h.getVacancyById)
		authorized.PUT("/vacancy/:id", h.updateVacancy)
		authorized.DELETE("/vacancy/:id", h.deleteVacancy)
	}

	// Открытые роуты
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	router.POST("/auth/login", h.Login)
	router.POST("/auth/register", h.Register)
	router.POST("/auth/changepassword", h.ChangePassword)

	return router
}
