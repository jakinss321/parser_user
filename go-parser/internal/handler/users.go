package handler

import (
	"main/models"
	"net/http"

	"github.com/gin-gonic/gin"
)

// @Summary Login
// @Description  Авторизация пользователя.
// @Accept       json
// @Produce      json
// @Param       User body models.LoginRequest true "Login user"
// @Success      200 {string} string "token"
// @Router       /auth/login [post]
func (h *Handler) Login(c *gin.Context) {
	var reqBody struct {
		Email    string `json:"email" binding:"required"`
		Password string `json:"password" binding:"required"`
	}

	if err := c.ShouldBindJSON(&reqBody); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	token, err := h.services.Login(reqBody.Email, reqBody.Password)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"token": token})
}

// @Summary Register
// @Description  Регистрация пользователя.
// @Accept       json
// @Produce      json
// @Param       User body models.RegisterRequest true "Add user"
// @Success      200 {integer} integer 1
// @Router       /auth/register [post]
func (h *Handler) Register(c *gin.Context) {
	var reqBody struct {
		Email          string `json:"email"`
		Password       string `json:"password"`
		RetypePassword string `json:"retype_password"`
	}
	if err := c.ShouldBindJSON(&reqBody); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	res, err := h.services.Register(reqBody.Email, reqBody.Password, reqBody.RetypePassword)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"res": res.Data})
}

// @Summary ChangePassword
// @Description  Смена пароля.
// @Accept       json
// @Produce      json
// @Param       ChangePasswordRequest body models.ChangePasswordRequest true "Change password"
// @Success      200 {integer} integer 1
// @Router       /auth/changepassword [post]
func (h *Handler) ChangePassword(c *gin.Context) {
	var req models.ChangePasswordRequest
	user, _ := c.Get("user")
	email := user.(*models.User).Email

	if err := c.ShouldBindJSON(&req); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	res, err := h.services.ChangePassword(email, req.OldPassword, req.NewPassword)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"res": res.Data})
}
