package handler

import (
	"main/models"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// @Summary CreateVacancy
// @Security ApiKeyAuth
// @Description  Создание вакансии.
// @Accept       json
// @Produce      json
// @Param        Vacancy body models.Vacancy true "Add vacancy"
// @Success      201  {object}  models.Vacancy
// @Router       /vacancy [post]
func (h *Handler) createVacancy(c *gin.Context) {
	var input models.Vacancy
	if err := c.BindJSON(&input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	err := h.services.CreateVacancy(&input)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusCreated, input)
}

type getAllVacanciesResponse struct {
	Data []*models.Vacancy `json:"data"`
}

// @Summary GetListVacancy
// @Security ApiKeyAuth
// @Description  Получаение списка вакансии.
// @Accept       json
// @Produce      json
// @Success      200  {object}  models.Vacancy
// @Router       /vacs [get]
func (h *Handler) getAllVacancies(c *gin.Context) {
	vacancies, err := h.services.GetAllVacancies()
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}
	c.JSON(http.StatusOK, getAllVacanciesResponse{
		Data: vacancies,
	})
}

// @Summary GetByIdVacancy
// @Security ApiKeyAuth
// @Description  Получаения вакансии по id.
// @Accept       json
// @Produce      json
// @Param        id   path      int  true  "Account ID"
// @Success      200  {object}  models.Vacancy
// @Router       /vacancy/{id} [get]
func (h *Handler) getVacancyById(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, "invalid id param")
		return
	}

	vacancy, err := h.services.GetVacancy(id)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}
	c.JSON(http.StatusOK, vacancy)
}

// @Summary DeleteByIdVacancy
// @Security ApiKeyAuth
// @Description  Удаление вакансии по id.
// @Accept       json
// @Produce      json
// @Param        id   path      int  true  "Account ID"
// @Success      200  {object}  models.Vacancy
// @Router       /vacancy/{id} [delete]
func (h *Handler) deleteVacancy(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, "invalid id param")
		return
	}

	err = h.services.DeleteVacancy(id)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}
	c.JSON(http.StatusOK, statusResponse{
		Status: "ok",
	})
}

// @Summary updateVacancy
// @Security ApiKeyAuth
// @Description  Редактирование вакансии.
// @Accept       json
// @Produce      json
// @Param        id   path  {object}  int  true  "id and model, Vacancy body models.Vacancy true"
// @Success      200  {object}  models.Vacancy
// @Router       /vacancy/{id} [put]

func (h *Handler) updateVacancy(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, "invalid id param")
		return
	}

	var input models.Vacancy
	if err := c.BindJSON(&input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	if err := h.services.UpdateVacancy(id, &input); err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, statusResponse{"ok"})
}
