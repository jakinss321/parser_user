package service

import (
	"main/models"
)

func (s *Services) CreateVacancy(vacancy *models.Vacancy) error {
	return s.repo.CreateVacancy(vacancy)
}

func (s *Services) GetVacancy(id int) (*models.Vacancy, error) {
	return s.repo.GetVacancy(id)
}

func (s *Services) GetAllVacancies() ([]*models.Vacancy, error) {
	return s.repo.GetAllVacancies()
}

func (s *Services) UpdateVacancy(id int, vacancy *models.Vacancy) error {
	return s.repo.UpdateVacancy(id, vacancy)
}

func (s *Services) DeleteVacancy(id int) error {
	return s.repo.DeleteVacancy(id)
}
