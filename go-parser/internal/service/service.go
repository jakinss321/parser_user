package service

import (
	"main/internal/repository"
	"main/internal/userclient"
	"main/models"
)

type Service interface {
	CreateVacancy(vacancy *models.Vacancy) error
	GetVacancy(id int) (*models.Vacancy, error)
	GetAllVacancies() ([]*models.Vacancy, error)
	UpdateVacancy(id int, vacancy *models.Vacancy) error
	DeleteVacancy(id int) error
	Login(email, password string) (*models.LoginResponse, error)
	Register(email, password, retypePassword string) (*models.RegisterResponse, error)
	ChangePassword(email, oldPassword, newPassword string) (*models.ChangePasswordResponse, error)
}

type Services struct {
	repo   repository.VacancyRepository
	client *userclient.UsersClient
}

func NewService(repo repository.VacancyRepository, baseURL string) Services {
	return Services{
		repo:   repo,
		client: userclient.NewUsersClient(baseURL),
	}
}
