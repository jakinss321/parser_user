package service

import (
	"main/models"
)

func (s *Services) Login(email, password string) (*models.LoginResponse, error) {
	return s.client.Login(email, password)
}

func (s *Services) Register(email, password, retypePass string) (*models.RegisterResponse, error) {
	return s.client.Register(email, password, retypePass)
}

func (s *Services) ChangePassword(email, oldPassword, newPassword string) (*models.ChangePasswordResponse, error) {
	return s.client.ChangePassword(email, oldPassword, newPassword)
}

// func (s *Services) AuthMiddleware() gin.HandlerFunc {
// 	return func(c *gin.Context) {
// 		token := c.GetHeader("Authorization")
// 		if token == "" {
// 			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
// 			return
// 		}

// 		// Проверка токена и получение информации о пользователе
// 		user, err := s.client.ValidateToken(token)
// 		if err != nil {
// 			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
// 			return
// 		}

// 		// Сохранение информации о пользователе в контексте запроса
// 		c.Set("user", user)
// 		c.Next()
// 	}
// }
