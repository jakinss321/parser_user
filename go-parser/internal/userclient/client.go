package userclient

import (
	"bytes"
	"encoding/json"
	"fmt"
	"main/models"
	"net/http"
)

type UsersClient struct {
	baseURL string
	client  *http.Client
}

func NewUsersClient(baseURL string) *UsersClient {
	return &UsersClient{
		baseURL: baseURL,
		client:  &http.Client{},
	}
}

func (s *UsersClient) Register(email, password, retypePassword string) (*models.RegisterResponse, error) {
	reqBody, err := json.Marshal(models.RegisterRequest{
		Email:          email,
		Password:       password,
		RetypePassword: retypePassword,
	})
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", fmt.Sprintf("%s/api/1/auth/register", s.baseURL), bytes.NewBuffer(reqBody))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	var resBody models.RegisterResponse
	err = json.NewDecoder(res.Body).Decode(&resBody)
	if err != nil {
		return nil, err
	}

	return &resBody, nil
}

func (s *UsersClient) Login(email, password string) (*models.LoginResponse, error) {
	// create the request body
	reqBody, err := json.Marshal(models.LoginRequest{
		Email:    "jakinss321@gmail.com",
		Password: "rank4743",
	})
	if err != nil {
		return nil, err
	}

	// create the request
	// req, err := http.NewRequest("POST", fmt.Sprintf("%s/api/1/auth/verify", ac.baseURL), nil)
	req, err := http.NewRequest("POST", fmt.Sprintf("%s/api/1/auth/login", s.baseURL), bytes.NewBuffer(reqBody))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")

	// send the request
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	fmt.Println(res.StatusCode, "статускод 1")
	// decode the response
	var resBody models.LoginResponse
	err = json.NewDecoder(res.Body).Decode(&resBody)
	if err != nil {
		return nil, err
	}

	return &resBody, nil
}

func (s *UsersClient) ChangePassword(newPassword, oldPassword, RetypeNewPassword string) (*models.ChangePasswordResponse, error) {
	// create the request body
	reqBody, err := json.Marshal(models.ChangePasswordRequest{
		OldPassword:       oldPassword,
		NewPassword:       newPassword,
		RetypeNewPassword: RetypeNewPassword,
	})
	if err != nil {
		return nil, err
	}

	// create the request
	req, err := http.NewRequest("POST", fmt.Sprintf("%s/api/1/auth/change", s.baseURL), bytes.NewBuffer(reqBody))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")

	// send the request
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	// decode the response
	var resBody models.ChangePasswordResponse
	err = json.NewDecoder(res.Body).Decode(&resBody)
	if err != nil {
		return nil, err
	}

	return &resBody, nil
}
