package middleware

import (
	"encoding/json"
	"errors"
	"fmt"
	"main/internal/component"
	"main/internal/userclient"
	"main/models"
	"net/http"

	"github.com/gin-gonic/gin"
)

type AuthMiddleware struct {
	responder         *component.Responder
	authServiceClient *userclient.UsersClient
}

func NewAuthMiddleware(responder *component.Responder, authServiceClient *userclient.UsersClient) *AuthMiddleware {
	return &AuthMiddleware{
		responder:         responder,
		authServiceClient: authServiceClient,
	}
}

func (am *AuthMiddleware) CheckStrict() gin.HandlerFunc {
	return func(c *gin.Context) {
		r := c.Request
		w := c.Writer
		token := r.Header.Get("Authorization")
		if token == "" {
			am.responder.Error(w, http.StatusUnauthorized, "no authorization header provided")
			c.Abort()
			return
		}

		user, err := ValidateAccessToken(token) //token
		if err != nil {
			am.responder.Error(w, http.StatusInternalServerError, "error during authorization check")
			c.Abort()
			return
		}

		if user == nil {
			fmt.Println(user)
			am.responder.Error(w, http.StatusUnauthorized, "invalid access token")
			c.Abort()
			return
		}

		c.Next()
	}
}

func ValidateAccessToken(token string) (*models.User, error) {
	user := models.User{}
	req, err := http.NewRequest("GET", "http://service2:8080/api/1/auth/user", nil)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", token)
	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	if resp.StatusCode == http.StatusOK {

		err = json.NewDecoder(resp.Body).Decode(&user)
		if err != nil {
			return nil, err
		}

		return &user, nil
	}
	return nil, errors.New("failed to validate access token")
}
