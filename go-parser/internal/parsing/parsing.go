package parsing

import (
	"encoding/json"
	"fmt"
	"log"
	"main/internal/service"
	"main/models"
	"net/http"
	"regexp"
	"strconv"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/tebeka/selenium"
	"github.com/tebeka/selenium/chrome"
)

const maxTries = 999999

type Parsing struct {
	services service.Service
}

func NewParsing(service service.Service) *Parsing {
	return &Parsing{services: service}
}

func (p *Parsing) Start() {
	fmt.Println("start")
	time.Sleep(30 * time.Second)
	// прописываем конфигурацию для драйвера
	caps := selenium.Capabilities{
		"browserName": "chrome",
	}

	// добавляем в конфигурацию драйвера настройки для chrome
	chrCaps := chrome.Capabilities{
		W3C: true,
	}
	caps.AddChrome(chrCaps)

	// переменная нашего веб драйвера
	var wd selenium.WebDriver
	var err error
	// прописываем адрес нашего драйвера

	urlPr := "http://selenium:4444/wd/hub"
	_ = selenium.DefaultURLPrefix
	// немного костылей чтобы драйвер не падал
	i := 1
	for i < maxTries {
		wd, err = selenium.NewRemote(caps, urlPr)
		if err != nil {
			log.Println(err)
			i++
			continue
		}
		break
	}
	// после окончания программы завершаем работу драйвера
	//

	page := 1
	query := "golang"

	p.nextPage(page, query, wd)

	vacCount, err := p.getVacCountRaw(wd)
	if err != nil {
		log.Println(err)
	}

	links, err := p.getVacLinks(wd)
	if err != nil {
		log.Println(err)
	}

	numPage := vacCount / len(links)

	for numPage >= page {
		page++
		link, err := p.getVacLinks(wd)
		if err != nil {
			log.Println(err)
		}
		links = append(links, link...)
		p.nextPage(page, query, wd)
		time.Sleep(time.Second)
	}
	wd.Close()
	for _, v := range links {
		p.exampleScrape(v)
	}
}

func (p *Parsing) getVacCountRaw(wd selenium.WebDriver) (int, error) {
	fmt.Println("getVacCountRaw")
	elem, err := wd.FindElement(selenium.ByCSSSelector, ".search-total")
	if err != nil {
		fmt.Println(err)
	}

	vacCountRaw, err := elem.Text()
	if err != nil {
		return 0, err
	}
	re := regexp.MustCompile("[0-9]+")
	numStr := re.FindString(vacCountRaw)
	num, err := strconv.Atoi(numStr)
	if err != nil {
		return 0, err
	}
	return num, nil

}

func (p *Parsing) getVacLinks(wd selenium.WebDriver) ([]string, error) {
	fmt.Println("getVacLinks")
	elems, err := wd.FindElements(selenium.ByCSSSelector, ".vacancy-card__title-link")
	if err != nil {
		return []string{}, err
	}
	var links []string
	for i := range elems {
		var link string
		link, err = elems[i].GetAttribute("href")
		if err != nil {
			continue
		}
		links = append(links, link)

	}
	return links, nil
}

func (p *Parsing) nextPage(page int, query string, wd selenium.WebDriver) {
	fmt.Println("nextPage")
	wd.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", page, query))
}

func (p *Parsing) exampleScrape(link string) {
	var vac models.Vacancy
	preUrl := "https://career.habr.com"
	res, err := http.Get(preUrl + link)
	if err != nil {
		fmt.Println(err)
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		log.Printf("status code error: %d %s", res.StatusCode, res.Status)
	}

	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Println(err)
	}

	dd := doc.Find("script[type=\"application/ld+json\"]")
	if dd == nil {
		log.Println("notFound")
	}

	err = json.Unmarshal([]byte(dd.Text()), &vac)
	if err != nil {
		panic(err)
	}

	err = p.services.CreateVacancy(&vac)
	if err != nil {
		panic(err)
	}
}
