package component

import (
	"encoding/json"
	"net/http"
)

type Responder struct {
}

func NewResponder() *Responder {
	return &Responder{}
}

func (r *Responder) Error(w http.ResponseWriter, status int, message string) {
	resp := make(map[string]string)
	resp["error"] = message

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(resp)
}
