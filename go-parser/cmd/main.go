package main

import (
	"context"
	"log"
	"main/internal/component"
	"main/internal/handler"
	"main/internal/parsing"
	"main/internal/repository"
	"main/internal/service"
	"main/internal/userclient"
	"main/server"
	"os"
	"os/signal"
	"syscall"

	_ "github.com/lib/pq"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"github.com/subosito/gotenv"
)

// @title           Documentation of your project API.
// @version         1.0

// @contact.name   API CRUD app

// @host      localhost:8085

// @securityDefinitions.apiKey ApiKeyAuth
// @in header
// @name Authorization

func main() {
	logrus.SetFormatter(new(logrus.JSONFormatter))

	if err := initConfig(); err != nil {
		logrus.Fatalf("error initializing configs: %s", err.Error())
	}

	if err := gotenv.Load(); err != nil {
		logrus.Fatalf("error loading env variables: %s", err.Error())
	}

	var db repository.VacancyRepository

	switch os.Getenv("DB") {
	case "postgres":
		cfg := repository.Config{
			Host:     viper.GetString("db.hostpostgres"),
			Port:     viper.GetString("db.postgresport"),
			Username: viper.GetString("db.username"),
			DBName:   viper.GetString("db.dbnamepostgres"),
			SSLMode:  viper.GetString("db.sslmode"),
			Password: os.Getenv("DB_PASSWORD"),
		}
		postgresDB, err := repository.NewPostgresDB(cfg)
		if err != nil {
			log.Fatalf("Failed to initialize db: %s", err.Error())
		}
		db, _ = repository.NewRepository(postgresDB, nil, nil)

	case "mongo":
		cfg := repository.Config{
			Host: viper.GetString("db.hostmongo"),
			Port: viper.GetString("db.port"),
		}
		mongoClient, err := repository.NewMongoClient(cfg)
		if err != nil {
			log.Fatalf("Failed to initialize db: %s", err.Error())
		}

		collection := mongoClient.Database("db-name").Collection(viper.GetString("db.collectionname"))
		db, _ = repository.NewRepository(nil, mongoClient, collection)

	default:
		panic("Unknown database type")
	}

	component := component.NewResponder()
	client := userclient.NewUsersClient("http://localhost:8080")
	services := service.NewService(db, viper.GetString("baseurlclientusers"))
	handlers := handler.NewHandler(services, component, client)
	_ = parsing.NewParsing(&services)
	// go parsingService.Start()
	srv := new(server.Server)
	go func() {
		if err := srv.Run(viper.GetString("port"), handlers.InitRoutes()); err != nil {
			logrus.Fatalf("error occured while running http server: %s", err.Error())
		}
		log.Printf("Server started on port %s", viper.GetString("port"))
	}()
	logrus.Info("Started")

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
	<-quit

	logrus.Info("Shutting Down")

	if err := srv.Shutdown(context.Background()); err != nil {
		logrus.Errorf("error occured on server shutting down: %s", err.Error())
	}

}

func initConfig() error {
	viper.AddConfigPath("configs")
	viper.SetConfigName("config")
	return viper.ReadInConfig()
}
