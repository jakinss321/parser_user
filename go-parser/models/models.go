// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse and unparse this JSON data, add this code to your project and do:
//
//    welcome, err := UnmarshalWelcome(bytes)
//    bytes, err = welcome.Marshal()

package models

type Vacancy struct {
	ID                 int         `json:"id" db:"id"`
	Context            string      `json:"@context" db:"context"`
	Type               string      `json:"@type" db:"type"`
	DatePosted         string      `json:"datePosted" db:"date_posted"`
	Title              string      `json:"title" db:"title" binding:"required"`
	Description        string      `json:"description" db:"description"`
	Identifier         interface{} `json:"identifier" db:"identifier"`
	ValidThrough       string      `json:"validThrough" db:"valid_through"`
	HiringOrganization interface{} `json:"hiringOrganization" db:"hiring_organization"`
	JobLocation        interface{} `json:"jobLocation" db:"job_location"`
	JobLocationType    string      `json:"jobLocationType" db:"job_location_type"`
	EmploymentType     string      `json:"employmentType" db:"employment_type"`
}

type User struct {
	ID            int    `json:"id"`
	Name          string `json:"name"`
	Phone         string `json:"phone"`
	Email         string `json:"email"`
	Password      string `json:"password"`
	Role          int    `json:"role"`
	Verified      bool   `json:"verified"`
	EmailVerified bool   `json:"email_verified"`
	PhoneVerified bool   `json:"phone_verified"`
}

type RegisterRequest struct {
	Email          string `json:"email"`
	Password       string `json:"password"`
	RetypePassword string `json:"retype_password"`
}

type RegisterResponse struct {
	Success   bool `json:"success"`
	ErrorCode int  `json:"error_code,omitempty"`
	Data      Data `json:"data"`
}

type Data struct {
	Message string `json:"message"`
}

type LoginRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type LoginResponse struct {
	Success   bool      `json:"success"`
	ErrorCode int       `json:"error_code,omitempty"`
	Data      LoginData `json:"data"`
}

type LoginData struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
	Message      string `json:"message"`
}

type ChangePasswordRequest struct {
	OldPassword       string `json:"old_password" binding:"required"`
	NewPassword       string `json:"new_password" binding:"required"`
	RetypeNewPassword string `json:"retype_new_password"`
}

type ChangePasswordResponse struct {
	Success   bool `json:"success"`
	ErrorCode int  `json:"error_code,omitempty"`
	Data      Data `json:"data"`
}

type ChangePasswordData struct {
	Message string `json:"message"`
}
